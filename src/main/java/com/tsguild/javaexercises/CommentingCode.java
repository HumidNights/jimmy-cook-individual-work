/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.javaexercises;

/**
 *
 * @author apprentice
 */
public class CommentingCode {
    public static void main(String[] args){
        // Comments are written to explain code in easily understandable way
        // Basically for single lines anything after a // is considered a comment
        System.out.println("Normal code is compiled and runs ...");
        System.out.println("Comments however ... "); // do not execute!
        
        // Comments can be on their own line
        System.out.println("..."); // Or they can share a line like this.
        
        // However if you put the // BEFORE a line of code
        // System.out.println("Then it is considered a comment, and won't execute!");
        /*
        
        This is a multi-line comment!
        Named because, well, it spans SO many lines!
        
        
        */
   

    }
}
